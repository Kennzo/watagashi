################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/controller.c \
../Src/encoder.c \
../Src/main.c \
../Src/motors.c \
../Src/potentiometer.c \
../Src/servo.c \
../Src/stm32f4xx_hal_msp.c \
../Src/stm32f4xx_it.c \
../Src/system_stm32f4xx.c 

OBJS += \
./Src/controller.o \
./Src/encoder.o \
./Src/main.o \
./Src/motors.o \
./Src/potentiometer.o \
./Src/servo.o \
./Src/stm32f4xx_hal_msp.o \
./Src/stm32f4xx_it.o \
./Src/system_stm32f4xx.o 

C_DEPS += \
./Src/controller.d \
./Src/encoder.d \
./Src/main.d \
./Src/motors.d \
./Src/potentiometer.d \
./Src/servo.d \
./Src/stm32f4xx_hal_msp.d \
./Src/stm32f4xx_it.d \
./Src/system_stm32f4xx.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o: ../Src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU GCC Compiler'
	@echo $(PWD)
	arm-none-eabi-gcc -mcpu=cortex-m4 -mthumb -mfloat-abi=hard -mfpu=fpv4-sp-d16 '-D__weak=__attribute__((weak))' '-D__packed="__attribute__((__packed__))"' -DUSE_HAL_DRIVER -DSTM32F411xE -I"/home/kennzo/Ac6/STMprojects/stm32f411re_Watagashi/Inc" -I"/home/kennzo/Ac6/STMprojects/stm32f411re_Watagashi/Drivers/STM32F4xx_HAL_Driver/Inc" -I"/home/kennzo/Ac6/STMprojects/stm32f411re_Watagashi/Drivers/STM32F4xx_HAL_Driver/Inc/Legacy" -I"/home/kennzo/Ac6/STMprojects/stm32f411re_Watagashi/Drivers/CMSIS/Device/ST/STM32F4xx/Include" -I"/home/kennzo/Ac6/STMprojects/stm32f411re_Watagashi/Drivers/CMSIS/Include"  -Og -g3 -Wall -fmessage-length=0 -ffunction-sections -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


