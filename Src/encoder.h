/*
 * encoder.h
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include "stm32f4xx_hal.h"

extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim5;

// encoder used in link (maybe connected to ENC1)
#define ENC_LINK_TIM_HANDLER htim4
#define ENC_LINK_TIM TIM4

// encoder used in sugerTank (maybe conneted to ENC2)
#define ENC_SUGER_TIM_HANDLER htim5
#define ENC_SUGER_TIM TIM5

#define ENC_RESOLUSION 4096 // 1024[PPR]*4
#define ENC_COUNT_DEFAULT 32768

typedef struct {
	TIM_HandleTypeDef *htim;
	TIM_TypeDef *TIM;
	uint32_t Channel;
	uint32_t angle;
}Encoder;

void encInit();
void encFin();
void linkEncCountReset();
void sugerEncCountReset();
int16_t linkEncReadCount();
int16_t sugerEncReadCount();
float linkEncGetAngle();

#endif /* ENCODER_H_ */
