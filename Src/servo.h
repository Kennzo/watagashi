/*
 * servo.h
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#ifndef SERVO_H_
#define SERVO_H_

#include "stm32f4xx_hal.h"

//pwm define
#define SERVO_TIM_HANDLER htim3
#define SERVO_TIM TIM3

// pin assign
#define SERVO1_CHANNEL TIM_CHANNEL_1
#define SERVO2_CHANNEL TIM_CHANNEL_2
#define SERVO3_CHANNEL TIM_CHANNEL_3
#define SERVO4_CHANNEL TIM_CHANNEL_4

// pwm setting
#define SERVO_COUNTER_PERIOD 1000
#define SERVO_PULSE_CYCLE 20 //[ms]

//pulse width
#define SERVO_MAX_PULSE 2.42 //[ms]
#define SERVO_MIN_PULSE 0.58 //[ms]

//value about hand
#define HAND_OPEN_ANGLE 135
#define HAND_CLOSE_ANGLE 85

#define YAW_UP_ANGLE 0
#define YAW_CENTER_ANGLE 90
#define YAW_DOWN_ANGLE 180

#define SUGER_RIGHT_POSI 0
#define SUGER_LEFT_POSI 180

extern TIM_HandleTypeDef htim3;

typedef struct {
	TIM_HandleTypeDef *htim;
	uint32_t Channel;
}Servo;

void servoInit();
uint16_t servo_map(uint16_t x, uint16_t min_in, uint16_t max_in, uint16_t min_out, uint16_t max_out);
void servo1_sendAngle(uint16_t angle);
void servo2_sendAngle(uint16_t angle);
void servo3_sendAngle(uint16_t angle);
void servo4_sendAngle(uint16_t angle);
void toggle_servo3();


#endif /* SERVO_H_ */
