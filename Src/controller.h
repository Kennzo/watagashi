/*
 * controller.h
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "stm32f4xx_hal.h"

#define CONTROLLER_SPI_HANDLER hspi2

//cs
#define CONTROLLER_GPIO GPIOB
#define CONTROLLER_GPIO_PIN GPIO_PIN_12

extern SPI_HandleTypeDef hspi2;

enum {
	CONTROLLER_LEFT     = 0b1000000000000000,
	CONTROLLER_DOWN     = 0b0100000000000000,
	CONTROLLER_RIGHT    = 0b0010000000000000,
	CONTROLLER_UP       = 0b0001000000000000,
	CONTROLLER_SQUARE   = 0b0000000010000000,
	CONTROLLER_CROSS    = 0b0000000001000000,
	CONTROLLER_CIRCLE   = 0b0000000000100000,
	CONTROLLER_TRIANGLE = 0b0000000000010000,
	CONTROLLER_L1       = 0b0000000000000100,
	CONTROLLER_L2       = 0b0000000000000001,
	CONTROLLER_R1       = 0b0000000000001000,
	CONTROLLER_R2       = 0b0000000000000010
};

typedef struct{
	int16_t x;
	int16_t y;
}analog_stick;

void controllerInit();
void controllerRead(uint16_t *buttons);
void controllerSelect();
void controllerDeselect();
void controllerDelay(uint16_t count);

#endif /* CONTROLLER_H_ */
