/*
 * servo.c
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#include "servo.h"
#include "stm32f4xx_hal.h"
#include <string.h>

Servo servo1 = {&SERVO_TIM_HANDLER, SERVO1_CHANNEL};
Servo servo2 = {&SERVO_TIM_HANDLER, SERVO2_CHANNEL};
Servo servo3 = {&SERVO_TIM_HANDLER, SERVO3_CHANNEL};
Servo servo4 = {&SERVO_TIM_HANDLER, SERVO4_CHANNEL};

const uint16_t servo_max_duty = SERVO_MAX_PULSE * SERVO_COUNTER_PERIOD / SERVO_PULSE_CYCLE;
const uint16_t servo_min_duty = SERVO_MIN_PULSE * SERVO_COUNTER_PERIOD / SERVO_PULSE_CYCLE;

void servoInit() {
	if(HAL_TIM_PWM_Start(servo1.htim, servo1.Channel) != HAL_OK) {
		Error_Handler();
	}

	if(HAL_TIM_PWM_Start(servo2.htim, servo2.Channel) != HAL_OK) {
		Error_Handler();
	}

	if(HAL_TIM_PWM_Start(servo3.htim, servo3.Channel) != HAL_OK) {
		Error_Handler();
	}

	if(HAL_TIM_PWM_Start(servo4.htim, servo4.Channel) != HAL_OK) {
		Error_Handler();
	}

}

uint16_t servo_map(uint16_t x, uint16_t min_in, uint16_t max_in, uint16_t min_out, uint16_t max_out) {
	uint16_t result = (x - min_in) * (max_out - min_out) / (max_in - min_in) + min_out;
	return result;
}


void servo1_sendAngle(uint16_t angle) {

	if(angle < 0) angle = 0;
	if(angle > 180) angle = 180;

	uint16_t duty = servo_map(angle, 0, 180, servo_min_duty, servo_max_duty);

	__HAL_TIM_SetCompare(servo1.htim, servo1.Channel, duty);
}

void servo2_sendAngle(uint16_t angle) {

	if(angle < 0) angle = 0;
	if(angle > 180) angle = 180;

	uint16_t duty = servo_map(angle, 0, 180, servo_min_duty, servo_max_duty);

	__HAL_TIM_SetCompare(servo2.htim, servo2.Channel, duty);
}

void servo3_sendAngle(uint16_t angle) {

	if(angle < 0) angle = 0;
	if(angle > 180) angle = 180;

	uint16_t duty = servo_map(angle, 0, 180, servo_min_duty, servo_max_duty);

	__HAL_TIM_SetCompare(servo3.htim, servo3.Channel, duty);
}

void servo4_sendAngle(uint16_t angle) {

	if(angle < 0) angle = 0;
	if(angle > 180) angle = 180;

	uint16_t duty = servo_map(angle, 0, 180, servo_min_duty, servo_max_duty);

	__HAL_TIM_SetCompare(servo4.htim, servo4.Channel, duty);
}

void toggle_servo3() {
	static pre_angle;
	if(pre_angle == SUGER_RIGHT_POSI) {
		servo3_sendAngle(SUGER_LEFT_POSI);
	} else {
		servo3_sendAngle(SUGER_RIGHT_POSI);
	}
}
