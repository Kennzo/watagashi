/*
 * controller.c
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#include "controller.h"

void controllerInit() {
	HAL_SPI_Init(&CONTROLLER_SPI_HANDLER);
	HAL_GPIO_WritePin(CONTROLLER_GPIO, CONTROLLER_GPIO_PIN, GPIO_PIN_SET);
}

void controllerSelect() {
	HAL_GPIO_WritePin(CONTROLLER_GPIO, CONTROLLER_GPIO_PIN, GPIO_PIN_RESET);
}

void controllerDeselect() {
	HAL_GPIO_WritePin(CONTROLLER_GPIO, CONTROLLER_GPIO_PIN, GPIO_PIN_SET);
}

void controllerDelay(uint16_t count) {
	for(uint16_t i = 0; i < count; i++);
}

void controllerRead(uint16_t* buttons) {

	controllerSelect();
	controllerDelay(1000);

	uint8_t tx_data[9] = {0x01, 0x42, 0x00, 0x00, 0x00};
	uint8_t rx_data[9] = {0x00, 0x00, 0x00, 0x00, 0x00};

	for (uint8_t i = 0; i < 5; i++) {
		HAL_SPI_TransmitReceive(&CONTROLLER_SPI_HANDLER, &tx_data[i], &rx_data[i], 1, 0xFFFF);
		controllerDelay(1000);
	}

	controllerDeselect();

	//rx_data[3] = ~rx_data[3];
	//rx_data[4] = ~rx_data[4];
	*buttons = ((rx_data[3] << 8) + rx_data[4]);

}

