/*
 * potentiometer.c
 *
 *  Created on: 2018/11/18
 *      Author: kennzo
 */

#include "potentiometer.h"

uint32_t buffer[ADCNUM];

void potentioInit() {
	if(HAL_ADC_Init(&POTENTIO_ADC_HANDLER) != HAL_OK) {
		Error_Handler();
	}
	if(HAL_ADC_Start_DMA(&POTENTIO_ADC_HANDLER, (uint32_t *)buffer, ADCNUM) != HAL_OK) { //quantity of adc
		  Error_Handler();
	}
}

uint16_t potentio_map(uint16_t x, uint16_t min_in, uint16_t max_in, uint16_t min_out, uint16_t max_out) {
	uint16_t result = (x - min_in) * (max_out - min_out) / (max_in - min_in) + min_out;
	return result;
}

uint16_t read_potentio_buf() {
	return buffer[0];
}

float read_potentio_angle() {
	float angle =  potentio_map(read_potentio_buf(), 0, 4095, 0, POTENTIO_MAX_ANGLE);
	return angle;
}
