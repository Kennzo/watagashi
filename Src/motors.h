/*
 * motors.h
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#ifndef MOTORS_H_
#define MOTORS_H_

#include "stm32f4xx_hal.h"
#include "encoder.h"

#define CONTROL_CYCLE 4 //[ms]

// for PWM setting
#define MOTOR_TIM_HANDLER htim1
#define MOTOR_TIM TIM1
#define MOTOR_LINK_CHANNEL TIM_CHANNEL_1
#define MOTOR_SUGER_CHANNEL TIM_CHANNEL_2
#define MOTOR_SUGER_SUPPLY_CHANNEL TIM_CHANNEL_3

#define PWM_LIMIT 800
#define LINK_PWM_LIMIT 400
#define SUGER_SUPPLY_PWM_LIMIT 900
// pin assign
#define MOTOR_LINK_GPIO1 GPIOB
#define MOTOR_LINK_GPIO1_PIN GPIO_PIN_0
#define MOTOR_LINK_GPIO2 GPIOB
#define MOTOR_LINK_GPIO2_PIN GPIO_PIN_1
#define MOTOR_SUGER_GPIO1 GPIOC
#define MOTOR_SUGER_GPIO1_PIN GPIO_PIN_4
#define MOTOR_SUGER_GPIO2 GPIOC
#define MOTOR_SUGER_GPIO2_PIN GPIO_PIN_5
#define MOTOR_SUGER_SUPPLY_GPIO1 GPIOA
#define MOTOR_SUGER_SUPPLY_GPIO1_PIN GPIO_PIN_7
#define MOTOR_SUGER_SUPPLY_GPIO2 GPIOA
#define MOTOR_SUGER_SUPPLY_GPIO2_PIN GPIO_PIN_6


#define GEAR_PITCH_DIAMETER 0.03

extern TIM_HandleTypeDef htim1;

typedef struct {
	GPIO_TypeDef *GPIO1;
	uint16_t GPIO_A_Pin;

	GPIO_TypeDef *GPIO2;
	uint16_t GPIO_B_Pin;

	TIM_HandleTypeDef *htim;
	uint32_t Channel;
}VNH5019;

typedef struct {
	const float p;
	const float d;
	const float i;
}PID;

void link_motor_sendCommand(int16_t duty);
void suger_motor_sendCommand(int16_t duty);
void suger_supply_motor_sendCommand(int16_t duty);
void link_motor_setAngle(float angle);
void suger_motor_v(float r_v);
uint16_t suger_motor_make_velmap(float accel, float v_max, float distance);
void suger_supply_motor_setAngle(float angle);
void motorsInit(void);
void suger_motor_trapezoid(uint16_t num, uint8_t mode);

#endif /* MOTORS_H_ */
