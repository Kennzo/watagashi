/*
 * potentiometer.h
 *
 *  Created on: 2018/11/18
 *      Author: kennzo
 */

#ifndef POTENTIOMETER_H_
#define POTENTIOMETER_H_

#include "stm32f4xx_hal.h"

#define POTENTIO_ADC_HANDLER hadc1
#define ADCNUM 3

#define POTENTIO_MAX_ANGLE 300

extern ADC_HandleTypeDef hadc1;

void potentioInit();
uint16_t read_potentio_buf();
float read_potentio_angle();



#endif /* POTENTIOMETER_H_ */
