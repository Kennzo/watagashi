/*
 * encoder.c
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#include "encoder.h"

// encoder used in link
Encoder enc_link = {&ENC_LINK_TIM_HANDLER, ENC_LINK_TIM, TIM_CHANNEL_ALL};
Encoder enc_suger = {&ENC_SUGER_TIM_HANDLER, ENC_SUGER_TIM, TIM_CHANNEL_ALL};

void encInit() {
	if(HAL_TIM_Encoder_Start(enc_link.htim, enc_link.Channel) != HAL_OK) {
		Error_Handler();
	}
	if(HAL_TIM_Encoder_Start(enc_suger.htim, enc_suger.Channel) != HAL_OK) {
		Error_Handler();
	}

	//reset the value
	enc_link.TIM->CNT = (uint16_t)ENC_COUNT_DEFAULT;
	enc_suger.TIM->CNT = (uint16_t)ENC_COUNT_DEFAULT;

}

void encFin() {
	if(HAL_TIM_Encoder_Stop(enc_link.htim, enc_link.Channel) != HAL_OK) {
			Error_Handler();
	}
	if(HAL_TIM_Encoder_Stop(enc_suger.htim, enc_suger.Channel) != HAL_OK) {
			Error_Handler();
	}

}

void linkEncCountReset() {
	enc_link.TIM->CNT = (uint16_t)ENC_COUNT_DEFAULT;
}

void sugerEncCountReset() {
	enc_suger.TIM->CNT = (uint16_t)ENC_COUNT_DEFAULT;
}

int16_t linkEncReadCount() {
	return enc_link.TIM->CNT - (int16_t)ENC_COUNT_DEFAULT;
}

int16_t sugerEncReadCount() {
	return -enc_suger.TIM->CNT + (int16_t)ENC_COUNT_DEFAULT;
}

float linkEncGetAngle() {
	int16_t position = linkEncReadCount();
	if(position >= 0) {
		return (float)(360 * position / (int16_t)ENC_RESOLUSION);
	} else if (position < 0) {
		return (float)(360 + 360 * position / (int16_t)ENC_RESOLUSION);
	} else {
		return 1.0f;
	}
}
