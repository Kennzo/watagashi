/*
 * motors.c
 *
 *  Created on: 2018/10/21
 *      Author: kennzo
 */

#include "motors.h"
#include "encoder.h"
#include "potentiometer.h"
#include "stm32f4xx_hal.h"
#include <math.h>

VNH5019 link_motor = {MOTOR_LINK_GPIO2, MOTOR_LINK_GPIO2_PIN,
					  MOTOR_LINK_GPIO1, MOTOR_LINK_GPIO1_PIN,
					  &MOTOR_TIM_HANDLER, MOTOR_LINK_CHANNEL};
VNH5019 suger_motor = {MOTOR_SUGER_GPIO1, MOTOR_SUGER_GPIO1_PIN,
					   MOTOR_SUGER_GPIO2, MOTOR_SUGER_GPIO2_PIN,
					   &MOTOR_TIM_HANDLER, MOTOR_SUGER_CHANNEL};
VNH5019 suger_supply_motor = {MOTOR_SUGER_SUPPLY_GPIO1, MOTOR_SUGER_SUPPLY_GPIO1_PIN,
					   MOTOR_SUGER_SUPPLY_GPIO2, MOTOR_SUGER_SUPPLY_GPIO2_PIN,
					   &MOTOR_TIM_HANDLER, MOTOR_SUGER_SUPPLY_CHANNEL};

PID linkMotor = {20.0f, 0.0f, 0.0f};
PID sugerMotor = {50.0f, 0.1f, 0.0f};   // p:20~30 d:max5~7
PID sugerSupplyMotor = {60.0f, 1.0f, 0.0f};   // p:20~30 d:max5~7

float velocity_map[500];

void link_motor_sendCommand(int16_t duty) {
	if(duty > 0) {
		if(duty > PWM_LIMIT) duty = LINK_PWM_LIMIT;
		HAL_GPIO_WritePin(link_motor.GPIO1, link_motor.GPIO_A_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(link_motor.GPIO2, link_motor.GPIO_B_Pin, GPIO_PIN_RESET);
		__HAL_TIM_SetCompare(link_motor.htim, link_motor.Channel, duty);
	} else if(duty < 0){
		if(duty < -PWM_LIMIT) duty = -LINK_PWM_LIMIT;
		HAL_GPIO_WritePin(link_motor.GPIO1, link_motor.GPIO_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(link_motor.GPIO2, link_motor.GPIO_B_Pin, GPIO_PIN_SET);
		__HAL_TIM_SetCompare(link_motor.htim, link_motor.Channel, -duty);
	} else {
		HAL_GPIO_WritePin(link_motor.GPIO1, link_motor.GPIO_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(link_motor.GPIO2, link_motor.GPIO_B_Pin, GPIO_PIN_RESET);
		__HAL_TIM_SetCompare(link_motor.htim, link_motor.Channel, duty);
	}
}

void suger_motor_sendCommand(int16_t duty) {
	if(duty > 0) {
		if(duty > PWM_LIMIT) duty = PWM_LIMIT;
		HAL_GPIO_WritePin(suger_motor.GPIO1, suger_motor.GPIO_A_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(suger_motor.GPIO2, suger_motor.GPIO_B_Pin, GPIO_PIN_RESET);
		__HAL_TIM_SetCompare(suger_motor.htim, suger_motor.Channel, duty);
	} else if(duty < 0){
		if(duty < -PWM_LIMIT) duty = -PWM_LIMIT;
		HAL_GPIO_WritePin(suger_motor.GPIO1, suger_motor.GPIO_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(suger_motor.GPIO2, suger_motor.GPIO_B_Pin, GPIO_PIN_SET);
		__HAL_TIM_SetCompare(suger_motor.htim, suger_motor.Channel, -duty);
	} else {
		HAL_GPIO_WritePin(suger_motor.GPIO1, suger_motor.GPIO_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(suger_motor.GPIO2, suger_motor.GPIO_B_Pin, GPIO_PIN_RESET);
		__HAL_TIM_SetCompare(suger_motor.htim, suger_motor.Channel, duty);
	}
}

void suger_supply_motor_sendCommand(int16_t duty) {
	if(duty > 0) {
		if(duty > PWM_LIMIT) duty = SUGER_SUPPLY_PWM_LIMIT;
		HAL_GPIO_WritePin(suger_supply_motor.GPIO1, suger_supply_motor.GPIO_A_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(suger_supply_motor.GPIO2, suger_supply_motor.GPIO_B_Pin, GPIO_PIN_RESET);
		__HAL_TIM_SetCompare(suger_supply_motor.htim, suger_supply_motor.Channel, duty);
	} else if(duty < 0){
		if(duty < -PWM_LIMIT) duty = -SUGER_SUPPLY_PWM_LIMIT;
		HAL_GPIO_WritePin(suger_supply_motor.GPIO1, suger_supply_motor.GPIO_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(suger_supply_motor.GPIO2, suger_supply_motor.GPIO_B_Pin, GPIO_PIN_SET);
		__HAL_TIM_SetCompare(suger_supply_motor.htim, suger_supply_motor.Channel, -duty);
	} else {
		HAL_GPIO_WritePin(suger_supply_motor.GPIO1, suger_supply_motor.GPIO_A_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(suger_supply_motor.GPIO2, suger_supply_motor.GPIO_B_Pin, GPIO_PIN_RESET);
		__HAL_TIM_SetCompare(suger_supply_motor.htim, suger_supply_motor.Channel, duty);
	}
}

void link_motor_setAngle(float angle) {
	static float pre_error;
	float now_angle = linkEncGetAngle();
	float error = angle - now_angle;

	if(error > 180) error = 360 - error;
	if(error < -180) error = 360 + error;

	int16_t duty = (int16_t)(linkMotor.p * error + (error - pre_error)*linkMotor.d*1000 / (int16_t)CONTROL_CYCLE);
	link_motor_sendCommand(duty);
	pre_error = error;
}

void suger_motor_v(float r_v) {
	static int16_t pre_pulse;
	static int16_t pre_duty;
	static int16_t pre_error;
	static float error_sum;
	int16_t pulse = sugerEncReadCount();
	float v = M_PI*GEAR_PITCH_DIAMETER*(pulse - pre_pulse)*1000/ENC_RESOLUSION/CONTROL_CYCLE;
	float error = v - r_v;
	error_sum += error * CONTROL_CYCLE / 1000;
	int16_t duty = pre_duty + sugerMotor.p * error +
			sugerMotor.d*(error - pre_error)*1000 / CONTROL_CYCLE + sugerMotor.i*error_sum;
	suger_motor_sendCommand(duty);
	pre_pulse = pulse;
	pre_duty = duty;
	pre_error = error;
}

void suger_supply_motor_setAngle(float angle) {
	static float pre_error;
	float now_angle = read_potentio_angle();
	float error = angle - now_angle;
	int16_t duty = (int16_t)(sugerSupplyMotor.p * error + (error - pre_error)*sugerSupplyMotor.d*1000 / (int16_t)CONTROL_CYCLE);
	suger_supply_motor_sendCommand(duty);
	pre_error = error;
}
uint16_t suger_motor_make_velmap(float accel, float v_max, float distance) {
	uint16_t time;
	uint16_t decel_time;
	//acceleration
	for(time = 0; time <= (uint8_t)(v_max/accel*1000/CONTROL_CYCLE); time++) {
		velocity_map[time] = accel*time*CONTROL_CYCLE/1000;
	}
	//reach the max speed
	for(; time <= (uint16_t)(distance/v_max*1000/CONTROL_CYCLE); time++) {
		velocity_map[time] = v_max;
	}
	decel_time = time;
	//deceleration
	for(; time <= (uint16_t)((distance/v_max + v_max/accel)*1000/CONTROL_CYCLE); time++) {
		velocity_map[time] = v_max - accel*(time-decel_time)*CONTROL_CYCLE/1000;
	}
	return time;

}

void suger_motor_trapezoid(uint16_t num, uint8_t mode) {
	if(mode == 0) {
	suger_motor_v(velocity_map[num]);
	} else {
		suger_motor_v(-velocity_map[num]);
	}
}
void motorsInit() {
	if(HAL_TIM_PWM_Start(link_motor.htim, link_motor.Channel) != HAL_OK) {
			Error_Handler();
	}
	if(HAL_TIM_PWM_Start(suger_motor.htim, suger_motor.Channel) != HAL_OK) {
		Error_Handler();
	}
	if(HAL_TIM_PWM_Start(suger_supply_motor.htim, suger_supply_motor.Channel) != HAL_OK) {
		Error_Handler();
	}

	link_motor_sendCommand(250);
	HAL_Delay(100);

	while(1) {
		if(0 <= linkEncGetAngle() && linkEncGetAngle() <= 3) {
			link_motor_sendCommand(0);
			break;
		} else {
			link_motor_sendCommand(250);
			HAL_Delay(1);
		}
	}
}


